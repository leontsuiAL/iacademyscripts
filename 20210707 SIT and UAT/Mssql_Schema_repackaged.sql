
USE db_iacademy
GO


-- -------------------------------------------------------------------------------
-- MODULE  : Mod_ConfigSchemaResource
-- VERSION : 2021070500
-- FILE    : ConfigSchemaResource\database\schema\2021070500.sql
-- -------------------------------------------------------------------------------

BEGIN
	IF UPPER(CONVERT(VARCHAR, SERVERPROPERTY('ServerName'))) IN ('HABDWD7CSQL01','HABD3D7CAPP02')
	BEGIN
		IF OBJECT_ID(N'dbo.Config_Config', N'U') IS NOT NULL
		AND EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE (TABLE_SCHEMA + '.' + TABLE_NAME) = N'dbo.Config_Config' AND COLUMN_NAME = N'module')
		AND EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE (TABLE_SCHEMA + '.' + TABLE_NAME) = N'dbo.Config_Config' AND COLUMN_NAME = N'name')
		BEGIN
			PRINT 'DEV UPDATED ON TABLE ==> dbo.Config_Config'
			
			UPDATE
			    dbo.Config_Config
			  SET
			    module = N'Training'
			  WHERE
			    name = N'Enable multi block registration'
			    AND module = N'Trainee Training'
			;
		END
	END
	ELSE IF UPPER(CONVERT(VARCHAR, SERVERPROPERTY('ServerName'))) IN ('HABQWQ7CSQL01','HKCSQWSQL72')
	BEGIN
		IF OBJECT_ID(N'aesprod.Config_Config', N'U') IS NOT NULL
		AND EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE (TABLE_SCHEMA + '.' + TABLE_NAME) = N'aesprod.Config_Config' AND COLUMN_NAME = N'module')
		AND EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE (TABLE_SCHEMA + '.' + TABLE_NAME) = N'aesprod.Config_Config' AND COLUMN_NAME = N'name')
		BEGIN
			PRINT 'UAT UPDATED ON TABLE ==> aesprod.Config_Config'
			
			UPDATE
			    aesprod.Config_Config
			  SET
			    module = N'Training'
			  WHERE
			    name = N'Enable multi block registration'
			    AND module = N'Trainee Training'
			;
		END
	END
	ELSE
	BEGIN
		IF OBJECT_ID(N'aesprod.Config_Config', N'U') IS NOT NULL
		AND EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE (TABLE_SCHEMA + '.' + TABLE_NAME) = N'aesprod.Config_Config' AND COLUMN_NAME = N'module')
		AND EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE (TABLE_SCHEMA + '.' + TABLE_NAME) = N'aesprod.Config_Config' AND COLUMN_NAME = N'name')
			BEGIN
			PRINT 'PROD UPDATED ON TABLE ==> aesprod.Config_Config'
			
			UPDATE
			    aesprod.Config_Config
			  SET
			    module = N'Training'
			  WHERE
			    name = N'Enable multi block registration'
			    AND module = N'Trainee Training'
			;
			END
	END
END
GO




-- Framework setup --

BEGIN
	IF UPPER(CONVERT(VARCHAR, SERVERPROPERTY('ServerName'))) IN ('HABDWD7CSQL01','HABD3D7CAPP02')
	BEGIN
		IF OBJECT_ID(N'dbo.CHARCOAL_VERSION', N'U') IS NOT NULL
		BEGIN
			PRINT 'DEV INSERTED INTO TABLE ==> dbo.CHARCOAL_VERSION'
			
			INSERT
			  INTO [dbo].[CHARCOAL_VERSION]
			    ( [Version] , [Filename] , [DateExecute] )
			  VALUES
			    ( N'2021070500' , N'ConfigSchemaResource\database\dev\2021070500.sql' , GETDATE( ) )
			;
		END
	END
	ELSE IF UPPER(CONVERT(VARCHAR, SERVERPROPERTY('ServerName'))) IN ('HABQWQ7CSQL01','HKCSQWSQL72')
	BEGIN
		IF OBJECT_ID(N'aesprod.CHARCOAL_VERSION', N'U') IS NOT NULL
		BEGIN
			PRINT 'UAT INSERTED INTO TABLE ==> aesprod.CHARCOAL_VERSION'
			
			INSERT
			  INTO [aesprod].[CHARCOAL_VERSION]
			    ( [Version] , [Filename] , [DateExecute] )
			  VALUES
			    ( N'2021070500' , N'ConfigSchemaResource\database\dev\2021070500.sql' , GETDATE( ) )
			;
		END
	END
	ELSE
	BEGIN
		IF OBJECT_ID(N'aesprod.CHARCOAL_VERSION', N'U') IS NOT NULL
			BEGIN
			PRINT 'PROD INSERTED INTO TABLE ==> aesprod.CHARCOAL_VERSION'
			
			INSERT
			  INTO [aesprod].[CHARCOAL_VERSION]
			    ( [Version] , [Filename] , [DateExecute] )
			  VALUES
			    ( N'2021070500' , N'ConfigSchemaResource\database\dev\2021070500.sql' , GETDATE( ) )
			;
			END
	END
END
GO



